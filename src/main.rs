fn main() {
    println!("Rust Programming Language Testing");
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        // The function body uses the assert_eq! macro to assert that 2 + 2 equals 4.
        assert_eq!(2 + 2, 4);
    }

    #[test]
    fn a_failure() {
        //  Tests fail when something in the test function panics.
        // Each test is run in a new thread,
        // and when the main thread sees that a test thread has died,
        // the test is marked as failed. 
        panic!("Make this test fail");
    }
}
